package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>{
    private SuiviViewModel model;

    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String question = model.getQuestions(position);
        holder.setQuestion(question);
        if (position < model.getNextQuestion()){
            System.out.println("POSITION : " + position + " ET NEXTQUESTION : " + model.getNextQuestion());
            ((CheckBox)holder.itemView.findViewById(R.id.checkBox)).setChecked(true);
        }else{
            ((CheckBox)holder.itemView.findViewById(R.id.checkBox)).setChecked(false);
        }
        if (position == model.getNextQuestion()){
            holder.itemView.findViewById(R.id.checkBox).setEnabled(true);
        }else{
            holder.itemView.findViewById(R.id.checkBox).setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return model.questions.length;
    }

    public void select(int position){
        Integer old = model.getNextQuestion();
        if (old != null) {
            notifyItemChanged(old);
        }
        model.setNextQuestion(position+1);
        notifyItemChanged(position+1);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        public void setQuestion(String question){
            ((TextView) itemView.findViewById(R.id.question)).setText(question);
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.itemView.findViewById(R.id.checkBox).setOnClickListener(view -> {
                select(getAdapterPosition());
            });
        }
    }

    // TODO Q6.a
    // TODO Q7
}


