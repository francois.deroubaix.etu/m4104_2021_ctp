package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

import java.lang.reflect.Array;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel viewModel;

    private Spinner spSalle;
    private Spinner spPoste;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        poste = "";
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        salle = DISTANCIEL;
        // TODO Q2.c
        viewModel = new ViewModelProvider(requireActivity()).get(SuiviViewModel .class);
        // TODO Q4
        spSalle = view.findViewById(R.id.spSalle);
        spPoste = view.findViewById(R.id.spPoste);
        spSalle.setAdapter(ArrayAdapter.createFromResource(getContext(),R.array.list_salles, android.R.layout.simple_spinner_item));
        spPoste.setAdapter(ArrayAdapter.createFromResource(getContext(),R.array.list_postes, android.R.layout.simple_spinner_item));

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText username = view.findViewById(R.id.tvLogin);
            viewModel.setUsername(username.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = getResources().getStringArray(R.array.list_salles)[position];
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = getResources().getStringArray(R.array.list_postes)[position];
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {            }
        });
        update();
        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        if (salle.equals(getResources().getStringArray(R.array.list_salles)[0])){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            viewModel.setLocalisation(salle);
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            viewModel.setLocalisation(salle+" " +poste);
        }

    }
    // TODO Q9
}